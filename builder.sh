#!/bin/bash

source src/file.sh
source src/say.sh
source _uri.sh

PRIMARY_MODULES=(
  'ask'
  'colors'
  'cursor'
  'db'
  'file'
  'handler'
  'os'
  'say'
  'screen'
  'string'
)

SECONDARY_MODULES=(
  'spinner' # ~ handler
)

TERTIARY_MODULES=(
  'repo' # ~ os,ask,spinner
  'package' # ~ os,ask,screen,spinner
  'url' # ~ os,ask,say,screen,handler
)

_help(){
  say.error "Options:\n\t\t-i <input.sh>\n\t\t-o <result.sh>"; exit 1;
}

[[ ${#} -ne 4 ]] && _help;

# handle args & assign source & result vars.
while [[ ${#} -gt 0 ]]; do
  case $1 in
    -i) 
      buildSourceFile="${2}";
      _uri.isfile "${buildSourceFile}";
      shift 2;
    ;;
    -o) 
      buildResultFile="${2}";
      [[ -n "${buildResultFile}" ]] || \
      { say.error "No such output variable."; exit 1; };
      shift 2;
    ;;
    *|?) 
      say.error "Unknown Option: '${1}'";
      _help;
    ;;
  esac
done

# adding chotu source code to result.
echo "$(_uri.chotuCode "${buildSourceFile}")" > "${buildResultFile}" && \
say.success "Module: 'code' is added."; 

# variable to store hash code.
HashCode=$'#@PRIMARY_MODULES\n#@SECONDARY_MODULES\n#@TERTIARY_MODULES\n#@OTHER_MODULES';
# variable to store hash code line.
HashCodePos=1;
# adding hash to file.
file.append.vert "${HashCode}" "${HashCodePos}" "${buildResultFile}" && \
say.success "Module: 'hash' is added.";

# This add shebang to output.
file.append.vert $'#!/bin/bash\n' "1" "${buildResultFile}" && \
say.success "Module: 'shebang' is added.";

# This is to adding main process.
while (( 1<2 )); do
  # checking source.
  _uri.hasSource "${buildResultFile}";
  # first used source.
  StartStrip="$(_uri.startSourceStrip "${buildResultFile}")" && \
  # position of first used source.
  StartStripPos="$(_uri.startSourceStrip.pos "${buildResultFile}")" && \
  # pop that source position.
  file.pop "${StartStripPos}" "${buildResultFile}";
  # add that source module.
  _uri.addModuleSource "${StartStrip}" "${buildResultFile}";
done
