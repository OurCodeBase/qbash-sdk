#!/bin/bash

Cd="$(dirname "${BASH_SOURCE[0]}")";

source ${Cd}/handler.sh

# source colors.sh
# source handler.sh

_spinner(){
  case $1 in
    start ) local Context=1; local Cols=2; local STRIP='⠇⠋⠙⠸⠴⠦';
      echo -ne "${2}"; printf "%${Cols}s";
      while : 
      do 
        printf "\b${STRIP:Context++%${#STRIP}:1}"; sleep 0.15;
      done
    ;;
    stop ) [[ -z ${3} ]] && { echo "${Red}${FUNCNAME}${Clear}: spinner is not running..." && exit 1; };
      kill ${3} > /dev/null 2>&1; echo -en "\b→ ";
      [[ $2 -eq 0 ]] && echo -e "[ ${Green}SUCCESS${Clear} ]" || \
      { echo -e "[ ${Red}FAILED${Clear} ]";
      setterm -cursor 'on'; exit 1; };
    ;;
  esac
  
}

# spinner.start(use,subject)
#   Starts spinner to spin.
# Args:
#   use (str) > takes process (eg: installing, processing).
#   subject (str) > takes subject (eg: file, function).
# Usage:
#   (use,subject) > (Processing 'Sleep')
spinner.start(){
  [[ ${#} -eq 2 ]] || { echo "${FUNCNAME}: invoke: 'missing args'" && return 1; };
  local ARGSPair="${Green}${FUNCNAME[1]}${Clear}: ${1} '${Green}${2}${Clear}'...";
  setterm -cursor 'off';
  _spinner start "${ARGSPair}" & _spinner_pid="${!}"
  disown
}

# spinner.stop()
#   Stops spinner to spin.
spinner.stop(){
  _spinner stop ${?} ${_spinner_pid};
  unset ${_spinner_pid};
  setterm -cursor 'on';
}
