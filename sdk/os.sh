#!/bin/bash

# source colors.sh

# os.isPlatform(os) ~ bool
#   Checks that you are using given platform.
# Args:
#   os (str) > eg: termux,userland,android,windows,etc.
os.isPlatform(){
  case $1 in
    'userland') ls '/host-rootfs/data/data/tech.ula/files/home' &>/dev/null;;
    'termux'|'android') ls '/data/data/com.termux/files/' &>/dev/null;;
    'linux') [[ "$(uname -srm)" == *"Linux"* ]];;
    'windows') [[ "$(uname -a)" == *"windows"* ]];;
    *) echo "${FUNCNAME}: No function available for your platform...";;
  esac
}

# os.isShell(shell) ~ bool
#   Check current shell.
# Args:
#   shell (str) > takes shell eg: bash,zsh,fish,etc.
os.isShell(){
  case $1 in
    'zsh') os.isPlatform 'userland' && \
      [[ "$(OurCodeBase-CShell)" == "zsh: "* ]] || \
      [[ "${SHELL}" == *"zsh" ]];
    ;;
    *) [[ "${SHELL}" == *"${1}" ]];;
  esac
}

# os.isCmdAvailable(Cmd,@--verbose) ~ bool
#   Checks that function is exist or not.
# Args:
#   Cmd (str) > takes function as string eg: python,node,etc.
#   --verbose (obj,optional) > explaination in brief.
os.isCmdAvailable(){
  command -v "${1}" &>/dev/null && return 0;
  case $2 in
    '--verbose') echo -e "${Red}${FUNCNAME}${Clear}: There is no '${Red}${1}${Clear}' function available..."; exit 1;;
    *) return 1;;
  esac
}