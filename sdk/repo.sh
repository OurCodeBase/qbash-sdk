#!/bin/bash

Cd="$(dirname "${BASH_SOURCE[0]}")";

source ${Cd}/os.sh
source ${Cd}/ask.sh
source ${Cd}/spinner.sh

# source os.sh
# source ask.sh
# source spinner.sh

# repo.size(api) ~ int
#   Used to get size of a repo.
# Args:
#   api (str) > takes api of github repo. (eg: "OurCodeBase/bash-sdk")
# Returns:
#   size (int) > gives you file size in MiB. (eg: 30)
repo.size(){
  os.isCmdAvailable 'curl' --verbose;
  local Api="$(echo "${1}" | awk '{print $1}')";
  local ApiSize=$(curl "https://api.github.com/repos/${Api}" 2> /dev/null | grep size | head -1 | tr -dc '[:digit:]');
  echo $(( ApiSize/1024 ));
}

# repo.chart(apis)
#   Used to view info of given repositories.
# Args:
#   apis (array) > takes array of repository api.
repo.chart(){
  screen.isSize '50' '12' --verbose;
  local ARGs=("${@}"); local PuraSize=0;
  echo -e "\n  ╭─ Clone ──────────────────────────────────────╮";
  echo -e "  │                                              │";
  printf "  │  %-34s %-7s  │\n" 'Repository' 'Size';
  printf "  │  %-34s %-7s  │\n" '──────────────────────────────────' '───────';
  for ARG in "${ARGs[@]}"; do
    local Api="$(echo "${ARG}" | awk '{print $1}')";
    local ApiSize="$(repo.size "${ARG}")";
    printf "  │  ${Green}%-34s${Clear} ${Yelo}%3s${Clear} %-3s  │\n" "${Api}" "${ApiSize}" 'MiB';
    local PuraSize=$(( PuraSize+ApiSize ));
  done
  echo -e "  │                                              │";
  echo -e "  ╰──────────────────────────────────────────────╯\n";
  echo -e "  ╭─ TOTAL ────────────────────╮";
  printf "  │  %14s: ${Green}%4s${Clear} %3s  │\n" "Cloning Size" "${PuraSize}" 'MiB';
  echo -e "  ╰────────────────────────────╯";
}

# repo.clone(apis,@dirs)
#   Used to start cloning of a repository.
# Args:
#   apis (array) > takes apis of github repo. (eg: OurCodeBase/bash-sdk)
#   dirs (array) > Optional: You can give directory path to clone to it.
repo.clone(){
  os.isCmdAvailable 'git' --verbose;
  local ARGs=("${@}"); repo.chart "${ARGs[@]}";
  ask.yesno "Cloning Repository";
  for ARG in "${ARGs[@]}"; do
    local Api="$(echo "${ARG}" | awk '{print $1}')";
    local ApiPath="$(echo "${ARG}" | awk '{print $2}')";
    local Url="https://github.com/${Api}.git";
    spinner.start "Cloning" "${Api}";
    [[ -z "${ApiPath}" ]] && \
    git clone --depth=1 "${Url}" 2> /dev/null || \
    git clone --depth=1 "${Url}" "${ApiPath}" 2> /dev/null;
    spinner.stop;
  done
  echo;
}
