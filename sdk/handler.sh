#!/bin/bash

# source colors.sh

# handler.guide()
#   A guide to interuption for user.
handler.guide(){
  echo -e "
  ╭────────────────────────────╮
  │  ♡ Press ${Red} CTRL+C ${Clear} to stop  │
  ╰────────────────────────────╯
    ";
}

# handler()
#   Handle interuption by user.
handler(){
  echo -e "\n${Red}${FUNCNAME}${Clear}: You stopped the process...\n";
  setterm -cursor 'on'; exit 1;
}

trap "handler" 2