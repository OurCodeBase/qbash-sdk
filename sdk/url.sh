#!/bin/bash

Cd="$(dirname "${BASH_SOURCE[0]}")";

source ${Cd}/os.sh
source ${Cd}/ask.sh
source ${Cd}/say.sh
source ${Cd}/screen.sh
source ${Cd}/handler.sh

# source os.sh
# source ask.sh
# source say.sh
# source screen.sh
# source handler.sh
# source color.sh

# url.contentSize(url) ~ int
#   Gives you size of content file in MiB.
# Args:
#   url (str) > takes url as string.
# Returns:
#   size (int) > size in MiB (eg: 60).
url.contentSize(){
  os.isCmdAvailable 'wget' --verbose;
  local ContentSizeVar="$(wget --spider "${1}" --no-check-certificate 2>&1)";
  local ContentSize="$(echo "${ContentSizeVar}" | grep -i length: | awk '{print $2}')";
  echo "$(( ContentSize/1048576 ))";
}

# url.contentChart(urls,@paths)
#   This is used to chart urls and size.
# Args:
#   urls (array) > takes one or array of urls.
#   paths (array,optional) > takes file paths.
url.contentChart(){
  screen.isSize '50' '12' --verbose;
  local ARGs=("${@}"); local PuraSize=0;
  echo -e "\n  ╭─ Content ────────────────────────────────────╮";
  echo -e "  │                                              │";
  printf "  │  %-34s %-7s  │\n" 'Content' 'Size';
  printf "  │  %-34s %-7s  │\n" '──────────────────────────────────' '───────';
  for ARG in "${ARGs[@]}"; do
    local ContentUrl="$(echo "${ARG}" | awk '{print $1}')";
    local ContentPath="$(echo "${ARG}" | awk '{print $2}')";
    [[ -z "${ContentPath}" ]] && \
    local ContentVar="$(echo "${ContentUrl}" | awk -F/ '{print $NF}')" || \
    local ContentVar="$(echo "${ContentPath}" | awk -F/ '{print $NF}')";
    local ContentSize="$(url.contentSize "${ContentUrl}")";
    printf "  │  ${Green}%-34s${Clear} ${Yelo}%3s${Clear} %-3s  │\n" "${ContentVar}" "${ContentSize}" 'MiB';
    local PuraSize=$(( PuraSize+ContentSize ));
  done
  echo -e "  │                                              │";
  echo -e "  ╰──────────────────────────────────────────────╯\n";
  echo -e "  ╭─ TOTAL ────────────────────╮";
  printf "  │  %14s: ${Green}%4s${Clear} %3s  │\n" "Download Size" "${PuraSize}" 'MiB';
  echo -e "  ╰────────────────────────────╯";
}

# url.retrive(urls,@paths)
#   This is used to get files via urls.
# Args:
#   urls (array) > takes one or array of url.
#   paths (array,optional) > takes files path.
url.retrive(){
  local ARGs=("${@}"); url.contentChart "${ARGs[@]}";
  os.isCmdAvailable 'wget' --verbose;
  ask.yesno 'Download Files';
  for ARG in "${ARGs[@]}"; do
    local ContentUrl="$(echo "${ARG}" | awk '{print $1}')";
    local ContentPath="$(echo "${ARG}" | awk '{print $2}')";
    [[ -z "${ContentPath}" ]] && \
    wget "${ContentUrl}" -q --show-progress --no-check-certificate || \
    wget -O "${ContentPath}" "${ContentUrl}" -q --show-progress --no-check-certificate;
    checkStatus;
  done
}
