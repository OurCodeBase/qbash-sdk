#!/bin/bash

# Supports only Yaml syntax database.

# _db.iskey(key,file) ~ bool
#   Checks that key is exist or not.
# Args:
#   key (str) > takes key of db.
#   file (str) > takes file path.
_db.iskey(){ grep "${1}: " "${2}" &>/dev/null; }

# db.read(key,file) ~ str
#   Give you data of key of yaml db.
# Args:
#   key (str) > takes key of db.
#   file (str) > takes file path.
db.read(){
  [[ ${#} -eq 2 ]] || { echo "${FUNCNAME}: invoke: 'missing args'"; return 1; };
  _db.iskey "${1}" "${2}" || { echo "${FUNCNAME}: ${1}: 'key not exists'"; return 1; };
  local dbValue="$(grep "${1}: " "${2}")";
  echo "${dbValue}" | awk -F': ' '{sub(/^[^:]*: /, ""); print}';
}

# db.create(key,value,file)
#   Adds data key and value to db.
# Args:
#   key (str) > takes key of db.
#   value (str) > takes value of key.
#   file (str) > takes file path.
db.create(){
  [[ ${#} -eq 3 ]] || { echo "${FUNCNAME}: invoke: 'missing args'"; return 1; };
  _db.iskey "${1}" "${3}" && { echo "${FUNCNAME}: ${1}: 'key already exists'"; return 1; };
  echo -ne "\n${1}: ${2}" >> "${3}";
}

# db.update(key,value,file)
#   Update data of key in db file.
# Args:
#   key (str) > takes key of db.
#   value (str) > takes update value of key.
#   file (str) > takes file path.
db.update(){
  [[ ${#} -eq 3 ]] || { echo "${FUNCNAME}: invoke: 'missing args'"; return 1; };
  _db.iskey "${1}" "${3}" || { echo "${FUNCNAME}: ${1}: 'key not exists'"; return 1; };
  local dbKey="${1}"; local dbUpdateValue="${2}"; local dbFile="${3}";
  local dbCurrentValue="$(db.read "${dbKey}" "${dbFile}")";
  local dbCurrentPair="${dbKey}: ${dbCurrentValue}";
  local dbCurrentPairPos="$(grep -n "${dbCurrentPair}" "${dbFile}" | cut -d: -f1 | head -n 1;)";
  local dbUpdatedPair="${dbCurrentPair/${dbCurrentValue}/"${dbUpdateValue}"}";
  sed -i "${dbCurrentPairPos}c\\${dbUpdatedPair}" "${dbFile}";
}

# db.delete(key,file)
#   Delete key and value of db file.
# Args:
#   key (str) > takes key of db.
#   file (str) > takes file path.
db.delete(){
  [[ ${#} -eq 2 ]] || { echo "${FUNCNAME}: invoke: 'missing args'"; return 1; };
  _db.iskey "${1}" "${2}" || { echo "${FUNCNAME}: ${1}: 'key not exists'"; return 1; };
  local dbKey="${1}: "; local dbFile="${2}";
  local dbKeyPos="$(grep -n "${dbKey}" "${dbFile}" | cut -d: -f1 | head -n 1;)";
  sed -i "${dbKeyPos}d" "${dbFile}";
}