#!/bin/bash

# current directory
Cd="$(dirname "${BASH_SOURCE[0]}")";

source ${Cd}/os.sh
source ${Cd}/ask.sh
source ${Cd}/screen.sh
source ${Cd}/spinner.sh

# source os.sh
# source ask.sh
# source screen.sh
# source spinner.sh

# pkg.size(@dnload,@install,package) ~ int
#   Gives you needed size of package.
# Args:
#   dnload (str,optional) > dnload to get file size.
#   install (str,optional) > install to get package installed size.
#   package (str) > takes package (eg: python,nodejs).
# Returns:
#   size (int) > Gives you size in MiBs.
# Usage:
#   pkg.size dnload package  > Gives you package file size.
#   pkg.size install package > Gives you package installed size.
pkg.size(){
  [[ ${#} -eq 2 ]] || { echo "${FUNCNAME}: invoke: 'missing args'"; return 1; };
  case $1 in
    'dnload') local SizeDataBase="$(apt show "${2}" 2> /dev/null | grep 'Download-Size:')";;
    'install') local SizeDataBase="$(apt show "${2}" 2> /dev/null | grep 'Installed-Size:')";;
  esac
  local Size="$(echo "${SizeDataBase}" | awk '{print $2}')";
  local SizeUnit="$(echo "${SizeDataBase}" | awk '{print $3}')";
  local Size="${Size%%.*}"; # decimals to integers.
  case "${SizeUnit}" in # check unit of package.
    'MB') echo "${Size}";;
    'kB') echo "$(( Size/1024 ))";;
    'B') echo "$(( Size/1048576 ))";;
  esac
}

# pkg.chart(pkgs)
#   Use to view chart of packages.
# Args:
#   pkgs (array) > takes array of packages.
pkg.chart(){
  screen.isSize '62' '12' --verbose; local ARGs=("${@}");
  local PuraSizeDL=0; local PuraSizeIN=0;
  echo -e "
  ╭─ Packages ─────────────────────────────────────────────╮";
  echo -e "  │                                                        │";
  printf "  │  %-25s %-10s %-7s %-7s  │\n" 'Package' 'Version' 'DLSize' 'INSize';
  printf "  │  %-25s %-10s %-7s %-7s  │\n" '─────────────────────────' '──────────' '───────' '───────';
  for ARG in "${ARGs[@]}"; do
    local PackageDataBase="$(apt show "${ARG}" 2> /dev/null)";
    local PackageNaam="$(echo "${PackageDataBase}" | grep 'Package:' | awk '{print $2}')";
    local PackageVersion="$(echo "${PackageDataBase}" | grep 'Version:' | awk '{print $2}' | awk -F'-' '{print $1}' | awk -F'+' '{print $1}' | awk -F'~' '{print $1}')";
    local PackageSizeDL="$(pkg.size 'dnload' "${ARG}")";
    local PackageSizeIN="$(pkg.size 'install' "${ARG}")";
    printf "  │  ${Green}%-25s${Clear} ${Yelo}%-10s${Clear} ${Yelo}%3s${Clear} %-3s ${Yelo}%3s${Clear} %-3s  │\n" "${PackageNaam}" "${PackageVersion}" "${PackageSizeDL}" 'MiB' "${PackageSizeIN}" 'MiB';
    local PuraSizeDL=$(( PuraSizeDL + PackageSizeDL ));
    local PuraSizeIN=$(( PuraSizeIN + PackageSizeIN ));
  done
  echo -e "  │                                                        │";
  echo -e "  ╰────────────────────────────────────────────────────────╯\n";
  echo -e "  ╭─ TOTAL ────────────────────╮";
  printf "  │  %14s: ${Green}%4s${Clear} %3s  │\n" "Download Size" "${PuraSizeDL}" 'MiB';
  printf "  │  %14s: ${Yelo}%4s${Clear} %3s  │\n" "Installed Size" "${PuraSizeIN}" 'MiB';
  echo -e "  ╰────────────────────────────╯";
}

# pkg.install(packages)
#   Used to install packages with good ui.
# Args:
#   packages (array) > takes packages as args. (eg: python nodejs neovim)
pkg.install(){
  local ARGs=("${@}"); pkg.chart "${ARGs[@]}";
  ask.yesno 'Install Packages';
  for ARG in "${ARGs[@]}"; do
    spinner.start 'Installing' "${ARG}";
    { os.isPlatform 'termux' || os.isShell 'zsh'; } && \
    apt-get install -qq "${ARG}" > /dev/null || \
    sudo apt-get install -qq "${ARG}" > /dev/null;
    spinner.stop;
  done
  echo;
}
