#!/bin/bash

# source colors.sh

# say.error(str) ~ str
say.error(){
  echo -e "${COLOR_STRIP[1]}${FUNCNAME[1]}${COLOR_STRIP[0]}: $1" | \
  sed -E "s/'([^']*)'/'\x1b[31m\1\x1b[0m'/g";
}

# say.debug(str) ~ str
say.debug(){
  echo -e "${COLOR_STRIP[3]}${FUNCNAME[1]}${COLOR_STRIP[0]}: $1" | \
  sed -E "s/'([^']*)'/'\x1b[33m\1\x1b[0m'/g";
}

# say.success(str) ~ str
say.success(){
  echo -e "${COLOR_STRIP[2]}${FUNCNAME[1]}${COLOR_STRIP[0]}: $1" | \
  sed -E "s/'([^']*)'/'\x1b[32m\1\x1b[0m'/g";
}

# checkStatus() ~ str
#   This prints success or failure according to exit code.
checkStatus(){
  [[ $? -eq 0 ]] && \
  echo -e " ~ [${COLOR_STRIP[2]} SUCCESS ${COLOR_STRIP[0]}]" || \
  { echo -e " ~ [${COLOR_STRIP[1]} FAILED ${COLOR_STRIP[0]}]" && exit 1; };
}
