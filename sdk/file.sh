#!/bin/bash

# Global returnable array
STRIP=()

# path.isdir(dir) ~ bool
#   Checks that directory exist or not.
# Args:
#   dir (str) > takes directory path.
path.isdir(){ test -d "${1}"; }

# path.isfile(file) ~ bool
#   Checks that file exist or not.
# Args:
#   file (str) > takes file path.
path.isfile(){ test -f "${1}"; }

# file.move(src,dst)
#   This can use to move files.
# Args:
#   src (str) > takes source path.
#   dst (str) > takes destination path.
file.move(){ mv -f "${1}" "${2}" 2>/dev/null; }

# file.copy(src,dst)
#   This can use to copy files.
# Args:
#   src (str) > takes source path.
#   dst (str) > takes destination path.
file.copy(){ cp -rf "${1}" "${2}" 2>/dev/null; }

# file.erase(file)
# Args:
#   file (str) > takes file path.
file.erase(){ truncate -s 0 "${1}"; }

# file.pop(pos,file)
#   Popout given position of line in file.
# Args:
#   pos (int) > takes position.
#   file (str) > takes file path.
file.pop(){ sed -i "${1}d" "${2}"; }

# file.readlines.int(file) ~ int
#   Gives you total lines of a file.
# Args:
#   file (str) > takes file path.
file.readlines.int(){ wc -l "${1}" | awk '{print $1}'; }

# file.readline(pos,file) ~ str
#   Gives you line from given position of file.
# Args:
#   pos (int) > takes position.
#   file (str) > takes file path.
file.readline(){ sed -n "${1}p" "${2}"; }

# file.readlines(file) ~ STRIP.
#   Gives you STRIP array of lines of given file.
# Args:
#   file (str) > takes file path.
file.readlines(){
  local ARGFile="${1}"; STRIP=();
  local ReadlinesCount=$(file.readlines.int "${ARGFile}");
  for ((i = 1; i <= ReadlinesCount; i++)); do
    STRIP+=("$(sed -n "${i}p" "${ARGFile}")");
  done
  export STRIP;
}

# file.readline.tall(file) ~ str
#   Gives you largest line of file.
# Args:
#   file (str) > takes file path.
file.readline.tall(){ awk 'length > max_length { max_length = length; max_line = $0 } END { print max_line }' "${1}"; }

# file.replace.pos(str,pos,file)
#   This replace text from given line of file.
# Args:
#   str (str) > takes string to replace.
#   pos (int) > takes position.
#   file (str) > takes file path.
file.replace.pos(){ sed -i "${2}c\\${1}" "${3}"; }

# file.search(str,file) ~ str
#   Search given text in file & return you that line.
# Args:
#   str (str) > takes string to search.
#   file (str) > takes file path.
file.search(){ grep "${1}" "${2}"; }

# file.search.pos(str,file) ~ pos
#   Search given text in file & return you position (eg: 1,2).
# Args:
#   str (str) > takes string to search.
#   file (str) > takes file path.
file.search.pos(){ grep -n "${1}" "${2}" | cut -d: -f1 | head -n 1; }

# path.dirArray(dir,@--by-time,@--no-extension) ~ STRIP.
#   Gives you array of files in given directory.
# Args:
#   dir (str) > takes directory path.
#   --by-time (obj,optional) > Optional: list will be sorted by time.
#   --no-extension (obj,optional) > Optional: list have no file extensions.
path.dirArray(){
  local ARGDir=${1};
  local ScribeFile=".scribe";
  path.isdir "${ARGDir}" || return 1;
  [[ "${*}" == *"--by-time"* ]] && \
  { ls -t "${ARGDir}" > "${ScribeFile}"; } || \
  ls "${ARGDir}" > "${ScribeFile}";
  [[ "${*}" == *"--no-extension"* ]] && \
  { local NoExtentionFiles="$(sed 's/\(.*\)\.\(.*\)/\1/' "${ScribeFile}")";
  echo "${NoExtentionFiles}" > "${ScribeFile}"; };
  file.readlines "${ScribeFile}"; rm "${ScribeFile}";
}

# file.append.hori(str,pos,file)
#   This appends text to file horizontally.
# Args:
#   str (str) > takes string to append it.
#   pos (int) > takes position.
#   file (str) > takes file path.
file.append.hori(){
  [[ ${#} -eq 3 ]] || { echo "${FUNCNAME}: invoke: 'missing args'" && return 1; };
  local String="${1}"; local Pos="${2}"; local ARGFile="${3}";
  local ConsVar="$(file.readline "${Pos}" "${ARGFile}")";
  local ConsVar+="${String}"; # concat
  file.replace.pos "${ConsVar}" "${Pos}" "${ARGFile}";
}

# file.append.vert(str,pos,file)
#   This appends text to file vertically.
# Args:
#   str (str) > takes string to append it.
#   pos (int) > takes position.
#   file (str) > takes file path.
file.append.vert(){
  [[ ${#} -eq 3 ]] || { echo "${FUNCNAME}: invoke: 'missing args'" && return 1; };
  local String="${1}"; local Pos="${2}"; # needs to reduce one.
  local ARGFile="${3}"; local Pos="$(( Pos - 1 ))";
  sed -i "${Pos}r /dev/stdin" "${ARGFile}" <<< "${String}";
}
