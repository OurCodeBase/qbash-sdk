#!/bin/bash

# cursor.set(on|off)
#   Switch terminal cursor easily.
cursor.set(){ setterm -cursor $1; }
