#!/bin/bash

# text.randize(array) ~ obj
#   Gives you random object of array.
# Args:
#   array (array) > takes array of objects.
text.randize(){ local ARGs=("$@"); echo "${ARGs[$(( RANDOM % ${#ARGs[@]} ))]}"; }

# text.isdigit(str) ~ bool
#   Checks string is digit or not.
# Args:
#   str (str) > takes string as arg.
text.isdigit(){ [[ "${1}" =~ ^[[:digit:]]+$ ]]; }

# text.replace(str,old,new) ~ str
#   This replace string to string.
# Args:
#   str (str) > takes string as arg.
#   old (str) > takes string to replace.
#   new (str) > takes string to replace with.
text.replace(){
  [[ ${#} -eq 3 ]] || { echo "${FUNCNAME}: invoke: 'missing args'" && return 1; };
  local String="${1}"; local OldString="${2}"; local NewString="${3}";
  echo "${String/${OldString}/"${NewString}"}";
}

# text.len(str,@--line) ~ int
#   Gives you lenth of given string.
# Args:
#   str (str) > takes string as arg.
#   --line (obj,optional) > to get count of lines.
text.len(){
  case $2 in
    '--line') echo "${1}" | wc -l ;;
    *) echo "${#1}" ;;
  esac
}

# text.startwith(str,startstr) ~ bool
#   Checks that string startswith given substring or not.
# Args:
#   str (str) > takes string as arg.
#   startstr (str) > takes substring as arg.
text.startwith(){ [[ "${1}" == "${2}"* ]]; }

# text.startwith(str,endstr) ~ bool
#   Checks that string endswith given substring or not.
# Args:
#   str (str) > takes string as arg.
#   endstr (str) > takes substring as arg.
text.endswith(){ [[ "${1}" == *"${2}" ]]; }

# text.contains(str,contain) ~ bool
#   Checks that string contains substring or not.
# Args:
#   str (str) > takes string as arg.
#   contain (str) > takes charachter.
text.contains(){ [[ "${1}" == *"${2}"* ]]; }

# text.charCount(str,char) ~ int
#   Gives you count of a given character in given string.
# Args:
#   str (str) > takes string as arg.
#   char (char) > takes charachter.
text.charCount(){ echo "${1}" | tr -cd "${2}" | wc -c; }

# text.replaceCharAt(str,pos,char) ~ str
#   Replace character of a string at given pos.
# Args:
#   str (str) > takes string as arg.
#   pos (int) > takes position.
#   char (char) > takes charachter.
text.replaceCharAt(){
  [[ ${#} -eq 3 ]] || { echo "${FUNCNAME}: invoke: 'missing args'" && return 1; };
  local String="${1}"; local Pos="${2}"; local Char="${3}";
  local PreString="${String:0:Pos-1}"; # substring before.
  local PostString="${String:Pos}"; # substring after.
  echo "${PreString}${Char}${PostString}";
}
