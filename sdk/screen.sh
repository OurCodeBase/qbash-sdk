#!/bin/bash

# screen.cols() ~ int
#   Current columns count in terminal.
screen.cols(){ stty size | awk '{print $2}'; }

# screen.lines() ~ int
#   Current lines count in terminal.
screen.lines(){ stty size | awk '{print $1}'; }

# screen.isSize(cols,lines,@--verbose) ~ bool
#   Checks that screen has atleast given lines and columns.
# Args:
#   cols (int) > takes columns as int.
#   lines (int) > takes lines as int.
#   --verbose (obj,optional) > explaination in brief.
screen.isSize(){
  [[ ${#} -ge 2 ]] || { echo "${FUNCNAME}: invoke: 'missing args'" && return 1; };
  local ARGCols=${1}; local ARGLines=${2};
  local CurrentCols="$(screen.cols)"; local CurrentLines="$(screen.lines)";
  (( CurrentCols >= ARGCols && CurrentLines >= ARGLines ));
  local ExitCode="${?}";
  case $3 in
    '--verbose') 
      (( ExitCode == 0 )) || \
      { echo -e "${Yelo}${FUNCNAME}${Clear}: Your Screen Size
      \t\tColumns: '${Yelo}${CurrentCols}${Clear}'
      \t\tLines: '${Yelo}${CurrentLines}${Clear}'";
      echo -e "${Green}${FUNCNAME}${Clear}: Require Screen Size
      \t\tColumns: '${Green}${ARGCols}${Clear}'
      \t\tLines: '${Green}${ARGLines}${Clear}'";
      echo -e "${Red}${FUNCNAME}${Clear}: Please '${Red}Zoom Out${Clear}' your Terminal
      \t\tThen run again.";
      exit 1; };
    ;;
    *) return "${ExitCode}";;
  esac
}