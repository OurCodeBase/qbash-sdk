#!/bin/bash

# source colors.sh

# Stores object that you selected.
ASKOBJ='';
# Stores position of object that you selected.
ASKPOS='';

# ask.user(title,@--capture) ~ bool
#   This takes yes & no for next process.
# Args:
#   title (str) > takes title (eg: You're Agree).
#   --capture (obj,optional) > capture input in 'ASKOBJ' variable.
ask.user(){
  read -p "
  > ${1}? [Y/n]:" ARG </dev/tty;
  case $ARG in
  ''|y|Y) return 0;;
    n|N) { echo -e "${Red}${FUNCNAME}${Clear}: Process stopped by user..."; exit 1; };;
    *) 
      [[ "${2}" == "--capture" ]] && \
      { ASKOBJ="${ARG}"; } || \
      { echo -e "${Red}${FUNCNAME}${Clear}: Please respond for yes or no..."; exit 1; };
    ;;
  esac
}

# ask.choose(title,list) ~ var
#   This creates a simple menu.
# Args:
#   title (str) > takes title (eg: Choose One).
#   list (array) > takes array as arg.
# Returns:
#   objects (var) > result in 'ASKOBJ' & 'ASKPOS' variables.
ask.choose(){
  [[ ${#} -ge 2 ]] || { echo "${FUNCNAME}: invoke: 'missing args'" && return 1; };
  PS3="
  ${1} :"; shift 1;
  local ARGs=("${@}");
  select ARG in "${ARGs[@]}"; do
    [[ "${REPLY}" =~ ^[[:digit:]]+$ ]] || \
    { echo -e "${Red}${FUNCNAME}${Clear}: Please enter '${Red}digits${Clear}' only..."; exit 1; };
    [[ "${REPLY}" -gt "${#ARGs[@]}" ]] && \
    { echo -e "${Red}${FUNCNAME}${Clear}: Please enter '${Red}valid${Clear}' digits..."; exit 1; };
    { ASKOBJ="${ARG}"; ASKPOS="${REPLY}"; break; };
  done
}
